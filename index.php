<?php
?>

<!DOCTYPE html>
<html>
<head>
    <link type="text/css" rel="stylesheet" href="css/navbar.css"  media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="css/main-content.css"  media="screen,projection"/>

    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>


    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body>
<?php
 include_once 'header.php';
?>

<div class="container">
    <!-- Page Content goes here -->
    <?php
        if(isset($_SESSION['u_id'])){
            echo "You are logged in!";
            echo $_SESSION['u_uid'];
        }
    ?>
    <div class="row">
        <div class="col s8">
            <div class="titel orange darken-2">Overzicht Tafels</div>
        </div>
        <div class="col s4">
        </div>
        <div class="row">
            <div class="col s8">
                <div class="row"
                    <div class="col s8">
                        <div class="row table-overview">
                            <div class="col s2">
                            </div>
                            <div class="col s2">
                                <div class="table"></div>
                            </div>
                            <div class="col s2">
                                <div class="table"></div>
                            </div>
                            <div class="col s2">
                                <div class="table"></div>
                            </div>
                            <div class="col s2">
                                <div class="table"></div>
                            </div>
                            <div class="col s2">
                            </div>
                        </div>
                        <div class="row table-overview">
                            <div class="col s2">
                            </div>
                            <div class="col s2">
                                <div class="table"></div>
                            </div>
                            <div class="col s2">
                                <div class="table"></div>
                            </div>
                            <div class="col s2">
                                <div class="table"></div>
                            </div>
                            <div class="col s2">
                                <div class="table"></div>
                            </div>
                            <div class="col s2">
                            </div>
                        </div>
                        <div class="row table-overview">
                            <div class="col s2">
                            </div>
                            <div class="col s2">
                                <div class="table"></div>
                            </div>
                            <div class="col s2">
                                <div class="table"></div>
                            </div>
                            <div class="col s2">
                                <div class="table"></div>
                            </div>
                            <div class="col s2">
                                <div class="table"></div>
                            </div>
                            <div class="col s2">
                            </div>
                        </div>
                        <div class="row table-overview">
                            <div class="col s2">
                            </div>
                            <div class="col s2">
                                <div class="table"></div>
                            </div>
                            <div class="col s2">
                                <div class="table"></div>
                            </div>
                            <div class="col s2">
                                <div class="table"></div>
                            </div>
                            <div class="col s2">
                                <div class="table"></div>
                            </div>
                            <div class="col s2">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col s4">
                    <div class="row">
                        <div class="col s12">
                            <div class="details orange darken-2"> Details </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include_once 'footer.php';
?>


<!--Import jQuery before materialize.js-->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="js/materialize.min.js"></script>
</body>
</html>