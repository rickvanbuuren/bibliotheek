<?php

if(isset($_POST['submit'])){
    include_once 'dbh.inc.php';

//    $first = $last = $email = $uid = $pwd = "";

    $first = mysqli_real_escape_string($conn, $_POST['first']);
    $last = mysqli_real_escape_string($conn, $_POST['last']);
    $email = mysqli_real_escape_string($conn, $_POST['email']);
    $uid = mysqli_real_escape_string($conn, $_POST['uid']);
    $pwd = mysqli_real_escape_string($conn, $_POST['pwd']);

//    if ($_SERVER["REQUEST_METHOD"] == "POST") {
//        $first = test_input($_POST['first']);
//        $last = test_input($_POST["last"]);
//        $email = test_input($_POST["email"]);
//        $uid = test_input($_POST["uid"]);
//        $pwd = test_input($_POST["pwd"]);
//    }

    if(empty($first) || empty($last) || empty($email) || empty($uid)|| empty($pwd)){
        header("Location: ../signup.php?signup=empty");
        exit();
    }else{
        if(!preg_match("/^[a-zA-Z]*$/", $first) || !preg_match("/^[a-zA-Z]*$/", $last)) {
            header("Location: ../signup.php?signup=invalid");
            exit();
        }else{
            if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
                header("Location: ../signup.php?signup=email");
                exit();
            }else{
                $sql = "SELECT * FROM users WHERE user_uid='$uid'";
                $result = mysqli_query($conn, $sql);
                $resultcheck = mysqli_num_rows($result);

                if($resultcheck > 0){
                    header("Location: ../signup.php?signup=usertaken");
                    exit();
                }
                else{
                    $hashedPwd = password_hash($pwd, PASSWORD_DEFAULT);

                    $sql = "INSERT INTO users (user_first, user_last, user_email, user_uid, user_pwd) VALUES ('$first', '$last', '$email', '$uid', '$hashedPwd');";

                    mysqli_query($conn, $sql);
                    header("Location: ../signup.php?signup=succes");
                    exit();
                }
            }
        }
    }

    function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

} else{
    header("Loaction: ../signup.php");
    exit();
}