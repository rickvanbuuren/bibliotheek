<?php
?>
<!DOCTYPE html>
<html>
<head>
    <?php
        include_once 'head.php';
    ?>
</head>

<body>
<?php
include_once 'header.php';
?>

<div class="container">
    <!-- Page Content goes here -->
    <div class="row">
        Sign Up
        <form class="signup-form" action="includes/signup.inc.php" method="POST">
            <input type="text" name="first" placeholder="Firstname">
            <input type="text" name="last" placeholder="Lastname">
            <input type="text" name="email" placeholder="E-mail">
            <input type="text" name="uid" placeholder="Username">
            <input type="password" name="pwd" placeholder="Placeholder">
<!--            <input type="text" name="first" placeholder="Firstname">-->
            <button type="submit" name="submit">Sign up</button>
        </form>
    </div>
</div>

<?php
include_once 'footer.php';
?>


<!--Import jQuery before materialize.js-->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="js/materialize.min.js"></script>
</body>
</html>
